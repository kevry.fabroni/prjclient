# Spécifie le compilateur C++
CXX = g++

# Options de compilation
CXXFLAGS = -std=c++11 -Wall

# Nom de l'exécutable à créer
TARGET = client

# Liste des fichiers sources à compiler
SOURCES = Client.cpp

# Règle par défaut : construire l'exécutable
all: $(TARGET)

# Règle pour créer l'exécutable
$(TARGET): $(SOURCES)
	$(CXX) $(CXXFLAGS) $(SOURCES) -o $(TARGET)

# Règle pour nettoyer les fichiers générés
clean:
	rm -f $(TARGET)
